<?php

class Najnowsze_Posty3 extends WP_Widget {

    /**
     * Sets up the widgets name etc
     */
    public function __construct() {
        $widget_id = 'najnowsze_posty3';
        $widget_name = __('Najnowsze Posty3', 'NajnowszePosty3');
        $widget_opt = array('description'=>'Ten widget wyświetla najnowsze posty.');

        parent::__construct($widget_id, $widget_name, $widget_opt);
    }

    /**
     * Outputs the content of the widget
     *
     * @param array $args
     * @param array $instance
     */
public function widget( $args, $instance ) {
    $title = apply_filters('widget_title', $instance['title']);

    if (empty($title))
    {
        $title = __('Najnowsze Posty3', 'NajnowszePosty3');
    }

    echo $args['before_widget'];

      //  echo $args['before_title'] . esc_html($title) . $args['after_title'];

        $query_args = array(
            'posts_per_page' => $instance['number'],
            'categories' => 'perełki',
            'post_status' => 'publish',
            'ignore_sticky_posts' => true,
            'orderby' => 'date',
            'order' => 'DESC'
        );
    
        $my_query = new WP_Query($query_args);

      
        if($my_query->have_posts()): ?>

            <ul class="lista">

                <?php while ($my_query->have_posts()): $my_query->the_post(); ?>
     
                                        
                    <div class="articles-widget-text"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                   
                    
                 
                <?php endwhile; ?>

            </ul>

        <?php else: ?>
            <p><?php _e('Nie znaleziono postów spełniających podane kryteria.'); ?></p>
        <?php endif;

        /* Restore original Post Data */
        wp_reset_postdata();

    echo $args['after_widget'];
}

    /**
     * Outputs the options form on admin
     *
     * @param array $instance The widget options
     */
    public function form( $instance ) {

        //ustawiamy opcje domyslne
        $my_defaults = array(
            'number' => 5,
            'title' => 'Najnowsze Posty3',
            'categories' => array(),
            'show_excerpt' => false,
            'excerpt_length' => 200,
            'show_thumbnail' => false
        );
        
        //uzupelniamy opcje zapisane w bazie o opcje domyslne
        $instance = wp_parse_args( (array) $instance, $my_defaults );

    ?>
        <p>
            <!-- Widgets name -->
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($instance['title']); ?>" />
        </p>
        <p>
            <!-- Number of posts to show -->
            <label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:' ); ?></label>
            <input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $instance['number']; ?>" size="3" />
        </p>
        <p>
            <!-- Get and display Categories checkbox options -->
            <label><?php _e('Categories:'); ?></label><br />
            <?php
            $categories_list = get_categories('hide_empty=0&orderby=name');
            foreach ($categories_list as $cat ):
                $cat_id = intval($cat->cat_ID);
                $cat_name = $cat->cat_name;
                $selected = '';

                if(in_array($cat_id, $instance['categories']))
                    $selected=' checked="checked"';    ?>

                <input value="<?php echo $cat_id; ?>" class="radio" type="checkbox"<?php echo $selected; ?> id="<?php echo $this->get_field_id('categories'); echo $cat_id; ?>" name="<?php echo $this->get_field_name('categories'); ?>[]" /> <label for="<?php echo $this->get_field_id('categories'); echo $cat_id; ?>"><?php echo $cat_name; ?></label><br />
            <?php endforeach; ?>
        </p>
        <p>
            <!-- Show excerpt -->
            <input class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('show_excerpt'); ?>" id="<?php echo $this->get_field_id('show_excerpt'); ?>" value="true" <?php checked(true, $instance['show_excerpt']);?> />
            <label for="<?php echo $this->get_field_id('show_excerpt'); ?>"> <?php _e( 'Display post excerpt' ); ?></label><br />
        </p>
        <p>
            <!-- Excerpt length -->
            <label for="<?php echo $this->get_field_id( 'excerpt_length' ); ?>"><?php _e( 'Excerpt length:' ); ?></label>
            <input id="<?php echo $this->get_field_id( 'excerpt_length' ); ?>" name="<?php echo $this->get_field_name( 'excerpt_length' ); ?>" type="text" value="<?php echo $instance['excerpt_length']; ?>" size="5" />
        </p>
        <p>
            <!-- Show excerpt -->
            <input class="checkbox" type="checkbox" name="<?php echo $this->get_field_name('show_thumbnail'); ?>" id="<?php echo $this->get_field_id('show_thumbnail'); ?>" value="true" <?php checked(true, $instance['show_thumbnail']);?> />
            <label for="<?php echo $this->get_field_id('show_thumbnail'); ?>"> <?php _e( 'Display post thumbnail' ); ?></label><br />
        </p>
    <?php    
    }

    /**
     * Processing widget options on save
     *
     * @param array $new_instance The new options
     * @param array $old_instance The previous options
     */
    public function update( $new_instance, $old_instance ) {
        
        $instance = $old_instance;

        //tytul widgetu
        $instance['title'] = strip_tags($new_instance['title']);
        //ilosc wyswietlanych postow
        $instance['number'] = absint($new_instance['number']);    
        //zaznaczone kategorie
        $instance['categories']  = (array) $new_instance['categories'];
        //czy pokazac wypis
        $instance['show_excerpt'] = isset($new_instance['show_excerpt']);
        //dlugosc wypisu
        $instance['excerpt_length'] = absint($new_instance['excerpt_length']);
        //czy pokazac miniaturke wpisu
        $instance['show_thumbnail'] = isset($new_instance['show_thumbnail']);

        return $instance;
    }
}