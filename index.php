<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package materialwp
 */

get_header(); ?>

   <div class="main-theme">  <div class="black-box">  </div>
<?php
$query_args = array(
            'posts_per_page' => '1',
            'post_status' => 'publish',
            'ignore_sticky_posts' => true,
            'orderby' => 'date',
            'order' => 'DESC'
        );
        $my_query = new WP_Query($query_args);
        if($my_query->have_posts()): ?>
             <?php while ($my_query->have_posts()): $my_query->the_post(); ?>
                                       
                              <div class="entry-meta-title-header">
				  <?php	$user_info = get_userdata(1);
				          echo get_the_date(). "\n";
                          echo $user_info->user_login . "\n";
                        ?>
			 	</div>

                    <div class="first-article-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
                                  
                          <div class="button-ninja-center">   <button class="button-ninja">PRZECZYTAJ CAŁY ARTYKUŁ  </button>  </div>

                <?php endwhile; ?>

        <?php else: ?>
            <p><?php _e('Nie znaleziono postów spełniających podane kryteria.'); ?></p>
        <?php endif;

        /* Restore original Post Data */
        wp_reset_postdata(); 
?>
     

           </div>


<div class="text-hot">  PEREŁKI  </div>

 <div class="containerX">   
 <div class="row">
 
        <div class="animated">
      <?php get_sidebar('next'); ?> 
         </div>

       <div class="col-md-9 margin-top">    <div class="navbar-style" style="letter-spacing: 4px;">
        NAJNOWSZE
        </div>   <?php get_sidebar('previous'); ?>   </div>

       <div class="col-md-3 margin-top-B">   <?php get_sidebar('van'); ?>   </div>

 </div>
 </div>



<?php get_footer(); ?>
