<?php
/**
 * @package materialwp
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="entry-img">
			<?php if ( has_post_thumbnail() ) : ?>
				<?php the_post_thumbnail(); ?>
			<?php endif; ?>
		</div>

		<div class="entry-container">
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

				<div class="entry-meta">
					<?php materialwp_posted_on(); ?>
				</div><!-- .entry-meta -->
			</header><!-- .entry-header -->

    <div class="container col-lg-8 margins-top-Alpha">

			<div class="entry-content">
				<?php the_content(); ?>
				<?php
					wp_link_pages( array(
						'before' => '<div class="page-links">' . __( 'Pages:', 'materialwp' ),
						'after'  => '</div>',
					) );
				?>
	  		</div><!-- .entry-content -->
        

 <div id="disqus_thread"></div>
<script>
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');
s.src = 'https://sp4-pl.disqus.com/embed.js';
s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>

    <div class="navbar-style" style="letter-spacing: 4px; margin-top: 100px;">
       PROPONOWANE ARTYKUŁY
        </div>

          <?php get_sidebar('article'); ?> 
</div>

                  </div>
         
                 <div class="col-lg-4 margins-top-Beta">

        <div class="navbar-style" style="letter-spacing: 4px;"> INSTAGRAM   </div>
                               <!-- SnapWidget -->
                               <div class="old-school" style="text-align: center;">
     <iframe src="https://snapwidget.com/embed/371445" class="snapwidget-widget" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:350px; height:390px; margin-top: 40px; text-align: center;"></iframe>
                              </div>
  
                         <?php get_sidebar('loft'); ?> 
                 </div>



			<footer class="entry-footer">
		
			</footer><!-- .entry-footer -->
		</div> <!-- .entry-container -->
        <!-- .card -->
</article><!-- #post-## -->
