<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package materialwp
 */
?>

	</div><!-- #content -->



  



	    <div class="footer_next"> 
	    <div class="footer-article" style="float: left; margin-left: 50px; font-size: 21px; color: #fff; padding-top: 40px;  font-family: 'Spectral', serif;  ">
	     Mateusz Faderewski
	     </div>
	    <ol class="footer-socialicons-list"> 
 	   <li>   <a href="https://www.twitter.com"> <i class="icon-twitter">            </i> </a>  </li>
	   <li>  <a href="https://www.instagram.com"> <i class="icon-instagram">         </i> </a> </li>
       <li> <a href="https://www.facebook.com">  <i class="icon-facebook-squared">   </i> </a> </li>  </ol>
	    </div>

	


</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
