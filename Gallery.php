<?php /* Template Name: Gallery */ ?>

<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package materialwp
 */

get_header(); ?>

<div class="background-picture-C">     </div>


<div class="container gallery-alfa">

	<?php while ( have_posts() ) : the_post(); ?>

			<div class="macro">	<?php the_content(); ?>  </div>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // end of the loop. ?>
	
	
</div> <!-- .container -->


<?php get_footer(); ?>