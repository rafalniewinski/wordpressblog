<?php
/**
 * materialwp functions and definitions
 *
 * @package materialwp
 */

if (!function_exists('materialwp_setup')):
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function materialwp_setup() {

	/**
	* Set the content width based on the theme's design and stylesheet.
	*/
	#global $content_width;
	if ( ! isset( $content_width ) ) {
		$content_width = 640; /* pixels */
	}

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on materialwp, use a find and replace
	 * to change 'materialwp' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'materialwp', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	//Suport for WordPress 4.1+ to display titles
	add_theme_support( 'title-tag' );

   	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	 add_theme_support( 'post-thumbnails' );


	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'materialwp' ),
	) );
   add_action( 'after_setup_theme', 'materialwp_setup' );
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	 add_theme_support( 'post-formats', array(
	 	'aside', 'image', 'video', 'quote', 'link',
	 ) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'materialwp_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // materialwp_setup
add_action( 'after_setup_theme', 'materialwp_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */

function materialwp_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'sidebar-next', 'twentyfifteen' ),
		'id'            => 'sidebar-next',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}

function materialwp_widgets_init2() {
	register_sidebar( array(
		'name'          => __( 'sidebar-previous', 'twentyfifteen' ),
		'id'            => 'sidebar-previous',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}

function materialwp_widgets_init3() {
	register_sidebar( array(
		'name'          => __( 'sidebar-van', 'twentyfifteen' ),
		'id'            => 'sidebar-van',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="navbar-style">',
		'after_title'   => '</h2>',
	));
}

function materialwp_widgets_init4() {
	register_sidebar( array(
		'name'          => __( 'sidebar-article', 'twentyfifteen' ),
		'id'            => 'sidebar-article',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}

function materialwp_widgets_init5() {
	register_sidebar( array(
		'name'          => __( 'sidebar-loft', 'twentyfifteen' ),
		'id'            => 'sidebar-loft',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'twentyfifteen' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="navbar-style">',
		'after_title'   => '</h2>',
	));
}



add_action( 'widgets_init', 'materialwp_widgets_init' );
add_action( 'widgets_init', 'materialwp_widgets_init2' );
add_action( 'widgets_init', 'materialwp_widgets_init3' );
add_action( 'widgets_init', 'materialwp_widgets_init4' );
add_action( 'widgets_init', 'materialwp_widgets_init5' );


/**
 * Enqueue scripts and styles.
 */
function materialwp_scripts() {
	wp_enqueue_style( 'mwp-bootstrap-styles', get_template_directory_uri() . '/bower_components/bootstrap/dist/css/bootstrap.min.css', array(), '3.3.6', 'all' );

	wp_enqueue_style( 'mwp-roboto-styles', get_template_directory_uri() . '/bower_components/bootstrap-material-design/dist/css/roboto.min.css', array(), '', 'all' );

	wp_enqueue_style( 'mwp-material-styles', get_template_directory_uri() . '/bower_components/bootstrap-material-design/dist/css/material-fullpalette.min.css', array(), '', 'all' );

	wp_enqueue_style( 'mwp-ripples-styles', get_template_directory_uri() . '/bower_components/bootstrap-material-design/dist/css/ripples.min.css', array(), '', 'all' );

	wp_enqueue_style( 'Index', get_template_directory_uri() . '/css/Index.css', array(), ' ', 'all' );

    wp_enqueue_style( 'header', get_template_directory_uri() . '/css/header.css', array(), '', 'all' );

    wp_enqueue_style( 'footer', get_template_directory_uri() . '/css/footer.css', array(), '', 'all' );

    wp_enqueue_style( 'pages', get_template_directory_uri() . '/css/pages2.css', array(), '', 'all' );

    wp_enqueue_style( 'sidebar', get_template_directory_uri() . '/css/sidebar.css', array(), '', 'all' );

    wp_enqueue_style( 'Kontakt', get_template_directory_uri() . '/css/Kontakt.css', array(), '', 'all' );

     wp_enqueue_style( 'Animate', get_template_directory_uri() . '/css/animate.css', array(), '', 'all' );

     wp_enqueue_style( 'fontello', get_template_directory_uri() . '/css/fontello/css/fontello.css', array(), '', 'all' );

    wp_enqueue_style( 'Single-Content', get_template_directory_uri() . '/css/Single-Content.css', array(), '', 'all' );

	wp_enqueue_style( 'materialwp-style', get_stylesheet_uri() );

	wp_enqueue_script( 'mwp-bootstrap-js', get_template_directory_uri() . '/bower_components/bootstrap/dist/js/bootstrap.min.js', array('jquery'), '3.3.6', true );

	wp_enqueue_script( 'mwp-ripples-js', get_template_directory_uri() . '/bower_components/bootstrap-material-design/dist/js/ripples.min.js', array('jquery'), '', true );

	wp_enqueue_script( 'mwp-material-js', get_template_directory_uri() . '/bower_components/bootstrap-material-design/dist/js/material.min.js', array('jquery'), '', true );
    
    wp_enqueue_script( 'viewcheck-js', get_template_directory_uri() . '/js/jquery.viewportchecker.js', array('jquery'), '', true );

	wp_enqueue_script( 'main-js', get_template_directory_uri() . '/js/main.js', array('jquery'), '', true );

	wp_enqueue_script( 'Background-js2', get_template_directory_uri() . '/js/Background-js2.js', array('jquery'), '3.3.6', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' )) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'materialwp_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Adds a Walker class for the Bootstrap Navbar.w
 */
require get_template_directory() . '/inc/bootstrap-walker.php';

/**
 * Comments Callback.
 */
require get_template_directory() . '/inc/comments-callback.php';



function najnowsze_posty_init()
{
    require get_template_directory() . '/widgety/najnowsze-posty.php';
    register_widget('Najnowsze_Posty');
    require get_template_directory() . '/widgety/vip.php';
    register_widget('vip');  
}


function poradywp_google_fonts() {
wp_register_style('googleFonts', 'https://fonts.googleapis.com/css?family=Source+Sans+Pro');
            wp_enqueue_style( 'googleFonts');
        }

add_action('wp_print_styles', 'poradywp_add_google_fonts');

add_action( 'widgets_init', 'najnowsze_posty_init' );